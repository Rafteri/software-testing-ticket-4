package com.kpfu.software.testing;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @Test
    public void getSum() {
        assertNotEquals(calculator.getSum(3, 4), 3. + 3., Calculator.DELTA);
        assertNotEquals(calculator.getSum(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY), 0., Calculator.DELTA);

        assertEquals(calculator.getSum(3, 4), 3. + 4., Calculator.DELTA);
        assertEquals(calculator.getSum(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY), Double.NaN, Calculator.DELTA);
    }

    @Test
    public void getSub() {
        assertNotEquals(calculator.getSub(3, 4), 3. - 3., Calculator.DELTA);
        assertNotEquals(calculator.getSub(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY), 0., Calculator.DELTA);

        assertEquals(calculator.getSub(3, 4), 3. - 4., Calculator.DELTA);
        assertEquals(calculator.getSub(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY), Double.NaN, Calculator.DELTA);
    }

    @Test
    public void getDivide() {
        assertNotEquals(calculator.getDivide(3, 4), 3. / 3., Calculator.DELTA);
        assertNotEquals(calculator.getDivide(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY), 1., Calculator.DELTA);

        assertEquals(calculator.getDivide(3, 4), 3. / 4., Calculator.DELTA);
        assertEquals(calculator.getDivide(3, 0.), Double.POSITIVE_INFINITY, Calculator.DELTA);
        assertEquals(calculator.getDivide(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY), Double.NaN, Calculator.DELTA);
    }

    @Test
    public void getMultiple() {
        assertNotEquals(calculator.getMultiple(3, 4), 3. * 3., Calculator.DELTA);
        assertNotEquals(calculator.getMultiple(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY), Double.NaN, Calculator.DELTA);

        assertEquals(calculator.getMultiple(3, 4), 3. * 4., Calculator.DELTA);
        assertEquals(calculator.getMultiple(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY), Double.POSITIVE_INFINITY, Calculator.DELTA);
    }
}