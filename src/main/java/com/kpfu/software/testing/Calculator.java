package com.kpfu.software.testing;

public class Calculator {

    public static final double DELTA = 0.0001d;

    public double getSum(final double a, final double b) {
        return a + b;
    }

    public double getSub(final double a, final double b) {
        return a - b;
    }

    public double getDivide(final double a, final double b) {
        return a / b;
    }

    public double getMultiple(final double a, final double b) {
        return a * b;
    }
}
